//
//  SchoolDetailView.swift
//  20220719-[DavidHakki]-NYCSchools
//
//  Created by Work on 7/19/22.
//

import SwiftUI

struct SchoolDetailView: View {
    @StateObject var satVM = SATScoresViewModel()
    @State var selectedSchool: School?
    
    var body: some View {
        VStack {
            
            
            List {
                
                Section("SAT:") {
                    
                    //Should use a recurring view if I have time. Unfortunately I ran out of time
                    HStack {
                        Label("Reading", systemImage: "book")
                            .bold()
                        Spacer()
                        Text(satVM.satScores?.satReading ?? "Missing")
                    }
                    
                    HStack {
                        Label("Writing", systemImage: "pencil")
                            .bold()
                        Spacer()
                        Text(satVM.satScores?.satWriting ?? "Missing")
                    }
                    
                    HStack {
                        Label("Math", systemImage: "divide")
                            .bold()
                        Spacer()
                        Text(satVM.satScores?.satMath ?? "Missing")
                    }
                }
                
                Section("School Mailing Information:") {
                    Text(selectedSchool?.school_name ?? "Missing Name")
                        .bold()
                    
                    VStack {
                        Text(selectedSchool?.primary_address_line_1 ?? "Missing Address")
                        Text("\(selectedSchool?.city ?? "") \(selectedSchool?.state_code ?? ""), \(selectedSchool?.zip ?? "")")
                    }
                }
                
            }
            
            Spacer()
        }
        .navigationTitle(selectedSchool?.school_name ?? "Test Academy")
        .task {
            if let dbn = selectedSchool?.dbn {
                await satVM.fetchScores(dbn: dbn)
            } else {
                //TODO: Proper error handling/alert
            }
        }
    }
}

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SchoolDetailView(selectedSchool: nil)
        }.navigationViewStyle(.stack)
    }
}
