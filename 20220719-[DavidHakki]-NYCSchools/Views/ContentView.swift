//
//  ContentView.swift
//  20220719-[DavidHakki]-NYCSchools
//
//  Created by Work on 7/19/22.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var schoolVM = SchoolViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                List(schoolVM.schools, id: \.dbn) { school in
                    VStack(alignment: .leading) {
                        Text(school.school_name)
                            .bold()
                        
                        Text(school.primary_address_line_1)
                            .font(.footnote)
                            .bold()
                        
                        
                        NavigationLink {
                            SchoolDetailView(selectedSchool: school)
                        } label: {
                           Text("Show Details")
                                .italic()
                        }
                        .buttonStyle(.borderedProminent)

                    }
                    
                    
                }
            }
            .navigationTitle("NYC Schools")
            .task {
                await schoolVM.fetchSchools()
            }
        }
        .navigationViewStyle(.stack)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
